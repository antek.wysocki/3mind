Wymagania dla środowiska linuksowego:

1. Stworzony kalatog np. "simple", w którym będzie przechowywany kod aplikacji. Do katalogu dostęp powinien mieć użytkownik serwera www.
2. Stworzona baza danych np. "simple". Użytkownik bazy danych: "simple".

Kroki to przetestowania aplikacji:

1. Wchodzimy do katalogu simple i pobieramy kod z repozytorium: 
git clone https://gitlab.com/antek.wysocki/3mind.git ./
2. Uruchamiamy composera: composer install
3. W pliku .env uzupełniamy dostęp do bazy danych, np: DATABASE_URL="mysql://simple:simple-Pass2021@127.0.0.1:3306/simple?serverVersion=5.7"
4. W terminalu wykonujemy komendę tworzącą bazę danych z migracji:
php bin/console doctrine:schema:create
5. Jeśli mamy zainstalowaną aplikację symfony, to wykonujemy polecenie np:
"symfony serve --no-tls" w celu uruchomienia aplikacji pod adresem localhost na porcie 8000:
http://127.0.0.1:8000