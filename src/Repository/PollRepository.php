<?php

namespace App\Repository;

use App\Entity\Poll;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Poll|null find($id, $lockMode = null, $lockVersion = null)
 * @method Poll|null findOneBy(array $criteria, array $orderBy = null)
 * @method Poll[]    findAll()
 * @method Poll[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PollRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Poll::class);
    }

    /**
     * @return Poll[] Returns an array of Poll objects created by User
     */
    public function getAllPolls(User $user = null)
    {
        $qb = $this->createQueryBuilder('p')
            ->andWhere('p.deletedAt is null');

        if ($user) {
            $qb = $qb->andWhere('p.createdBy = :user')->setParameter('user', $user);
        }

        $qb = $qb->orderBy('p.finishedAt', 'DESC')
            ->addOrderBy('p.createdAt', 'ASC')
            ->getQuery()
            ->getResult()
        ;

        return $qb;
    }
}
