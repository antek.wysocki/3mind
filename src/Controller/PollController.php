<?php

namespace App\Controller;

use App\Entity\Poll;
use App\Form\PollFirstType;
use App\Form\PollSecondType;
use App\Repository\PollRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PollController extends AbstractController
{
	protected $em;
    protected $repPoll;

	public function __construct(EntityManagerInterface $em, PollRepository $repPoll)
	{
		$this->em = $em;
        $this->repPoll = $repPoll;
	}


    /**
     * @Route("/polls", name="poll_list")
     */
    public function pollList(Request $request): Response
    {
        $user = $this->getUser();

        $polls = $this->repPoll->getAllPolls($user);

        return $this->render('poll/list.html.twig', [
            'polls' => $polls, 
            'user' => $user
        ]);
    }

    /**
     * @Route("/poll-start", name="poll")
     */
    public function poll(Request $request, Poll $poll = null): Response
    {
    	if (!$poll) {
	    	$poll = new Poll();
            $poll->setCreatedAt(new \DateTime());
            $poll->setCreatedBy($this->getUser());
    	}
    	$form = $this->createForm(PollFirstType::class, $poll);

    	$form->handleRequest($request);

    	if ($form->isSubmitted() and $form->isValid()) {
    		$this->em->persist($poll);
    		$this->em->flush();
    		return $this->redirectToRoute('poll_complete', array('id' => $poll->getId()));
    	}

        return $this->render('poll/first.html.twig', [
            'form' => $form->createView()
        ]);
    }


    /**
     * @Route("/poll-end/{id}", name="poll_complete")
     */
    public function pollComplete(Poll $poll, Request $request): Response
    {
    	if (!$poll) {
    		return $this->redirectToRoute("poll");
    	}
    	$form = $this->createForm(PollSecondType::class, $poll);

    	$form->handleRequest($request);

    	if ($form->isSubmitted() and $form->isValid()) {
            $poll->setFinishedAt(new \DateTime());
    		$this->em->persist($poll);
    		$this->em->flush();
            $this->addFlash('success', 'Dziękujemy za wypełnienie ankiety');
    		return $this->redirectToRoute('poll_list');

    	}

        return $this->render('poll/second.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/pol-delete/{id}", name="poll_delete")
     */
    public function pollDelete(Request $request, Poll $poll): Response
    {
        if (!$poll) {
            return $this->redirectToRoute("poll");
        }

        $poll->setDeletedAt(new \DateTime());
        $this->em->persist($poll);
        $this->em->flush();
        return $this->redirectToRoute('poll_list');

    }
}
